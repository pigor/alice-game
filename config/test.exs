import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :alice_game, AliceGameWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "6BgU58G0+Jb8Egk3TSDrlRoNRlXwhdZAqUMriFv9CZ23FYvPoBRy2MBdAFGcA/EA",
  server: false

# In test we don't send emails.
config :alice_game, AliceGame.Mailer, adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
