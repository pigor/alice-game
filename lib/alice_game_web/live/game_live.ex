defmodule AliceGameWeb.GameLive do
  use AliceGameWeb, :live_view

  alias AliceGame.Enigmas
  alias AliceGameWeb.FinishLive

  def mount(_params, _session, socket) do
    step = Enigmas.get_question(0)

    {:ok,
     socket
     |> assign(:title, "<h3>Bem Vindo ou Bem Vinda!</h3>")
     |> assign(:step, 0)
     |> assign(:instructions, step.instructions)
     |> assign(:enigma, step)}
  end

  def handle_event("validate", %{"enigma" => enigma}, socket) do
    case Enigmas.validate(enigma["step"], enigma) do
      {:ok, _step, :done} ->
        {:noreply,
        socket
        |> put_flash(:info, gettext("Você conseguiu!!!"))
        |> redirect(to: Routes.live_path(socket, FinishLive, "4l1c364n0s"))}

      {:ok, step, next_enigma} ->
        {:noreply,
        socket
        |> assign(:title, "")
        |> assign(:step, step)
        |> assign(:instructions, next_enigma.instructions)
        |> assign(:enigma, next_enigma)}

      {:error, _} ->
        handle_error(socket)
    end
  end

  def handle_error(socket) do
    {:noreply,
     socket
     |> redirect(to: Routes.live_path(socket, __MODULE__))}
  end
end
