defmodule AliceGameWeb.FinishLive do
  use AliceGameWeb, :live_view

  def mount(%{"code" => "4l1c364n0s"} = _params, _session, socket) do
    {:ok, socket}
  end

  def mount(params, _session, socket) do
    IO.inspect(params)
    {:noreply,
      socket
      |> put_flash(:error, gettext("Você precisa desvendar o enigma e conseguir o código!"))
      |> redirect(to: Routes.live_path(socket, __MODULE__))}
  end
end
