defmodule AliceGame.Enigmas do
  @enigmas [
    %{
      instructions: """
        Você precisa desvendar cada etapa para chegar ao
        enigma final, que revelará o mistério para você encontrar
        o presente do <b>Encanto da Alice</b>!
        <br>
        A Alice adora charadas, enigmas e tudo começou com brincadeiras de
        esconder objetos para achar, mapas e charadas do clássico
        O QUE É? O QUE É?... então vamos aquecer com algumas de suas favoritas!
      """,
      question: "O que é? o que é? Qual o cachorro que não tem rabo?",
      answer: "cachorro-quente"
    },
    %{
      instructions: """
        Muito bem, mais uma para garantir que não foi pura sorte!
      """,
      question: "O que é? o que é? Qual o animal que tem patas na cabeça?",
      answer: "piolho"
    },
    %{
      instructions: """
        Boa, vamos ver se você está por dentro do tema da festa!
      """,
      question: "Qual o personagem que a Alice mais gosta do filme Encanto?",
      answer: "mirabel"
    },
    %{
      instructions: """
        Você está se saindo muito bem, mas essa era fácil! Vamos ver a próxima.
      """,
      question: "Quais os nomes das personagens do filme que possuem os poderes relacionados a: força, plantas e cura? (Dica: separe os nomes por espaço)",
      answer: "luiza isabela julieta"
    },
    %{
      instructions: """
        Você é bom nisso, vamos ver o que você sabe sobre a Alice.
      """,
      question: "Qual a série de investigação que a Alice adora? (Dica, é do canal Gloob)",
      answer: "dpa"
    },
    %{
      instructions: """
        Essa era fácil, vamos ver como você se sai nessa. :)
      """,
      question: "Quais os nomes das melhores amigas da Alice?",
      answer: "alice e laura"
    },
    %{
      instructions: """
        Muito bem, você sabia que a Alice também tem um lugar especial que ela gosta de ir?
      """,
      question: "Qual a cidade que a Alice adora visitar?",
      answer: "tucuruí"
    },
    %{
      instructions: """
        Parabéns!!! Agora vamos para a última!
        Você deve saber que no filme todos dizem que a Mirabel não tem um dom,
        quando chegou a vez dela de revelar o dom, a porta não abriu. Mas a casa mágica
        do Encanto revela o quanto ela é especial, como ela mesma diz: <b>"Com dom ou sem dom, eu sou muito especial!"</b>
        <br>
        Para revelar o presente do <b>Encanto da Alice</b> você precisa responder a última pergunta.
      """,
      question: "A Mirabel tem sim um dom, esse dom é revelado no final e tá relacionado a algo muito importante, qual a palavra-chave relacionado ao dom da Mirabel?",
      answer: "família"
    }
  ]

  def get_question(index) when is_binary(index) do
    {num, _} = Integer.parse(index)

    get_question(num)
  end

  def get_question(index) do
    Enum.at(@enigmas, index) |> Map.put(:answer, "")
  end

  def validate(index, enigma) when is_binary(index) do
    {num, _} = Integer.parse(index)

    validate(num, enigma)
  end

  def validate(index, %{"question" => question, "answer" => answer} = _enigma) do
    @enigmas
    |> Enum.at(index, :done)
    |> validate(index, question, answer)
  end

  def validate(_index, _enigma), do: {:error, :reset}

  def validate(:done, _index, _question, _answer), do: {:ok, -1, :done}

  def validate(enigma, index, question, answer) do
    if(question == enigma[:question] && String.downcase(answer) == enigma[:answer]) do
      next_step = index + 1
      {:ok, next_step, Enum.at(@enigmas, next_step, :done)}
    else
      {:error, {index, enigma}}
    end
  end
end
